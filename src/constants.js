export default {
    TEETER_TOTTER_WIDTH: 10,
    MAX_SIDES_DIFFERENCE: 20,
    MAX_WEIGHT: 10
}
