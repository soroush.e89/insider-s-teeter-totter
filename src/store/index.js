import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isPaused: true,
    rightObject: [],
    leftObject: [],
    fallingObject: []
  },
  mutations: {
    SET_RIGHT(state, data) {
      state.rightObject.push(data);
    },
    SET_LEFT(state, data) {
      state.leftObject.push(data);
    },
    SET_Falling(state, data) {
      state.fallingObject=data;
    },
  },
  actions: {
    setLeftObject: ({ commit, state }, newValue) => {
      commit('SET_LEFT', newValue)
      return state.leftObject;
    },
    setRightObject: ({ commit, state }, newValue) => {
      commit('SET_RIGHT', newValue)
      return state.rightObject;
    },
    setFallingObject: ({ commit, state }, newValue) => {
      commit('SET_Falling', newValue)
      return state.fallingObject;
    }
  },
  modules: {
  }
})
