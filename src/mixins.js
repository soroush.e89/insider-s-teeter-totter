import { v4 } from 'uuid'
import constant from './constants'

export function getObjects() {

    const id = v4();
    const type = Math.floor(Math.random() * 3);
    const weight = Math.floor(Math.random() * constant.MAX_WEIGHT) + 1;
    const distanceFromCenter = Math.floor(Math.random() * constant.TEETER_TOTTER_WIDTH / 2) + 1;
    return {
        id,
        type,
        weight,
        distanceFromCenter
    }
}
export function getFalling() {
    let Falling = [];
    const FallingCount = Math.ceil(Math.random() * 3);
    for (let i = 0; i < FallingCount; i++) {
        Falling.push(getObjects());
    }
    return Falling;
}
